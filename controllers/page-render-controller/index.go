package pagerendercontroller

import (
	"encoding/json"
	"errors"
	"fmt"
	"go-frontend-service-template/services/jsinterpreter"
	pagescache "go-frontend-service-template/services/pages-cache"
	rpcservice "go-frontend-service-template/services/rpc-service"
	errorsutils "go-frontend-service-template/utils/errors"
	httpheadersutils "go-frontend-service-template/utils/http-headers"
	"log"
	"net/http"
	"strings"
	"time"
)

type TPagesRenderController struct {
	pagesCache       *pagescache.TPagesCache
	jsinterpreter    *jsinterpreter.TJsInterpreter
	htmlBase         string
	acceptLangParser *httpheadersutils.TAcceptLanguageParser
	rpcService       *rpcservice.TRpcService
}

func New(
	pagesCache *pagescache.TPagesCache,
	jsinterpreter *jsinterpreter.TJsInterpreter,
	htmlBase string,
	rpcService *rpcservice.TRpcService,
) *TPagesRenderController {
	return &TPagesRenderController{
		pagesCache:       pagesCache,
		jsinterpreter:    jsinterpreter,
		htmlBase:         htmlBase,
		acceptLangParser: httpheadersutils.NewAcceptLangParser(),
		rpcService:       rpcService,
	}
}

func (self *TPagesRenderController) renderDefaultPageImpl(
	wrt http.ResponseWriter,
	req *http.Request,
) {
	wrt.Header().Add("Content-Type", "text/html")
	wrt.WriteHeader(200)
	wrt.Write([]byte(self.htmlBase))
}

func (self *TPagesRenderController) RenderDefaultPage(
	wrt http.ResponseWriter,
	req *http.Request,
) {
	self.renderDefaultPageImpl(wrt, req)
}

func (self *TPagesRenderController) getPackedResource(
	pageGetResource func() (interface{}, error),

) (string, error) {
	resource, err := pageGetResource()
	if err != nil {
		return "", err
	}
	if resource != nil {
		packedResource, err := json.Marshal(resource)
		if err != nil {
			return "", err
		}
		return string(packedResource), nil
	}

	return "", nil
}

func (self *TPagesRenderController) getStatePatch(
	pageName string,
	pageSsrArgs []interface{},
) (string, error) {
	statePatchFn := fmt.Sprintf(
		"ssr%vStatePatch",
		pageName,
	)
	statePatch, err := self.jsinterpreter.ExecExtFunction(
		statePatchFn,
		pageSsrArgs...,
	)
	if err != nil {
		return "", err
	}
	statePatchStr, isStatePatchStr := statePatch.(string)
	if !isStatePatchStr {
		return "", errors.New("state patch is not a string")
	}

	return statePatchStr, nil
}

func (self *TPagesRenderController) getRenderedHtmlCss(
	pageName string,
	pageSsrArgs []interface{},
) (map[string]string, error) {
	pageFn := fmt.Sprintf(
		"ssr%vPage",
		pageName,
	)
	rawPage, err := self.jsinterpreter.ExecExtFunction(
		pageFn,
		pageSsrArgs...,
	)
	if err != nil {
		return nil, err
	}
	page, isPageMap := rawPage.(map[string]interface{})
	if !isPageMap {
		return nil, errors.New("page is not an object with html & cssTags")
	}

	if _, hasHtml := page["html"]; !hasHtml {
		return nil, errors.New("page doesn't have 'html' string")
	}
	if _, isHtmlString := page["html"].(string); !isHtmlString {
		return nil, errors.New("page's 'html' is not a string")
	}

	if _, hasCssTags := page["cssTags"]; !hasCssTags {
		return nil, errors.New("page doesn't have 'cssTags' string")
	}
	if _, isCssTagsString := page["cssTags"].(string); !isCssTagsString {
		return nil, errors.New("page's 'cssTags' is not a string")
	}

	return map[string]string{
		"html":    page["html"].(string),
		"cssTags": page["cssTags"].(string),
	}, nil
}

func (self *TPagesRenderController) getRenderedPage(
	statePatch string,
	renderedHtml string,
	renderedCss string,
) string {
	htmlWithPatch := strings.Replace(
		self.htmlBase,
		"<script id=\"state-patch-root\"></script>",
		statePatch,
		1,
	)
	html := strings.Replace(
		htmlWithPatch,
		"<div id=\"root\"></div>",
		fmt.Sprintf(
			"<div id=\"root\">%v</div>",
			renderedHtml,
		),
		1,
	)
	fullHtml := strings.Replace(
		html,
		"<style id=\"sc-placeholder-root\"></style>",
		renderedCss,
		1,
	)

	return fullHtml
}

func (self *TPagesRenderController) renderPage(
	wrt http.ResponseWriter,
	req *http.Request,
	pageName string,
	pageCacheDuration time.Duration,
	pageGetResource func() (interface{}, error),
) {
	pathWQuery := ""
	if len(req.URL.RawQuery) > 0 {
		pathWQuery = fmt.Sprintf(
			"%v?%v",
			req.URL.Path,
			req.URL.RawQuery,
		)
	} else {
		pathWQuery = req.URL.Path
	}

	cachedPage, hasCachedPage := self.pagesCache.GetPage(pathWQuery)
	if hasCachedPage {
		wrt.Header().Add("Content-Type", "text/html")
		wrt.WriteHeader(200)
		wrt.Write([]byte(cachedPage))
		return
	}

	acceptLanguage := req.Header.Get("Accept-Language")
	language := self.acceptLangParser.GetLanguageCode(acceptLanguage)
	pageSsrArgs := []interface{}{language}

	resource, err := self.getPackedResource(
		pageGetResource,
	)
	if err != nil {
		log.Println(
			fmt.Sprintf(
				"[%v/%v/ERROR-ON-FETCH-RESOURCE] => %v",
				"PageRenderController",
				fmt.Sprintf(
					"Page%vRender",
					pageName,
				),
				err.Error(),
			),
		)
		self.renderDefaultPageImpl(wrt, req)
		return
	}
	if len(resource) > 0 {
		pageSsrArgs = append(pageSsrArgs, resource)
	}

	statePatch, err := self.getStatePatch(pageName, pageSsrArgs)
	if err != nil {
		errorsutils.WriteErrToResponse(
			wrt,
			err,
			"PagesRenderController",
			fmt.Sprintf(
				"Render%vPage",
				pageName,
			),
		)
		return
	}

	pageSsrArgsForPage := make([]interface{}, 0)
	for _, arg := range pageSsrArgs {
		pageSsrArgsForPage = append(pageSsrArgsForPage, arg)
	}
	pageSsrArgsForPage = append(pageSsrArgsForPage, req.URL.Path)
	if len(req.URL.RawQuery) > 0 {
		pageSsrArgsForPage = append(pageSsrArgsForPage, req.URL.RawQuery)
	}
	htmlCss, err := self.getRenderedHtmlCss(pageName, pageSsrArgsForPage)
	if err != nil {
		errorsutils.WriteErrToResponse(
			wrt,
			err,
			"PagesRenderController",
			fmt.Sprintf(
				"Render%vPage",
				pageName,
			),
		)
		return
	}

	page := self.getRenderedPage(statePatch, htmlCss["html"], htmlCss["cssTags"])
	self.pagesCache.PutPage(
		pathWQuery,
		page,
		pageCacheDuration,
	)

	wrt.Header().Add("Content-Type", "text/html")
	wrt.WriteHeader(200)
	wrt.Write([]byte(page))
}

func (self *TPagesRenderController) RenderExamplePageA(
	wrt http.ResponseWriter,
	req *http.Request,
) {
	getResource := func() (interface{}, error) {
		resource, err := self.rpcService.GetExampleResource()
		return resource, err
	}

	self.renderPage(
		wrt,
		req,
		"Example",
		time.Hour*3,
		getResource,
	)
}
