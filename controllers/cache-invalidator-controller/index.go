package cacheinvalidatorcontroller

import (
	pagescache "go-frontend-service-template/services/pages-cache"
	"net/http"
)

type TCacheInvalidatorController struct {
	pagesCache   *pagescache.TPagesCache
	ipcSecretKey string
}

func New(
	pagesCache *pagescache.TPagesCache,
	ipcSecretKey string,
) *TCacheInvalidatorController {
	return &TCacheInvalidatorController{
		pagesCache:   pagesCache,
		ipcSecretKey: ipcSecretKey,
	}
}

func (self *TCacheInvalidatorController) InvalidateExamplePage(
	wrt http.ResponseWriter,
	req *http.Request,
) {
	ipcSecret := req.Header.Get("x-ipc-secret")
	if ipcSecret != self.ipcSecretKey {
		wrt.WriteHeader(403)
		wrt.Write([]byte("Not authorized"))
		return
	}

	self.pagesCache.InvalidatePage("/example")
	wrt.WriteHeader(200)
	wrt.Write([]byte("OK"))
}
