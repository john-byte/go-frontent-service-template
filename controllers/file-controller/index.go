package filecontroller

import (
	errors "go-frontend-service-template/utils/errors"
	files "go-frontend-service-template/utils/files"
	"io/ioutil"
	"net/http"
	"path/filepath"
)

type TFileController struct {
	uploadRoot string
}

var extensionToMime map[string]string = map[string]string{
	"png":  "image/png",
	"jpg":  "image/jpg",
	"gif":  "image/gif",
	"svg":  "image/svg+xml",
	"ico":  "image/x-icon",
	"mp3":  "audio/mpeg",
	"ogg":  "audio/ogg",
	"html": "text/html; charset=UTF-8",
	"js":   "application/javascript; charset=UTF-8",
	"css":  "text/css; charset=UTF-8",
	"json": "application/json",
	"txt":  "text/plain",
	"ttf":  "application/x-font-ttf",
	"otf":  "application/x-font-opentype",
}

func New(uploadRoot string) TFileController {
	return TFileController{
		uploadRoot: uploadRoot,
	}
}

func (self *TFileController) FetchFile(
	wrt http.ResponseWriter,
	req *http.Request,
) {
	path := req.URL.Path

	filePath := filepath.Join(
		self.uploadRoot,
		path,
	)
	extension := files.GetFileExtension(filePath)
	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		errors.WriteErrToResponse(
			wrt,
			err,
			"FileController",
			"FetchFile",
		)
		return
	}

	mime, hasMime := extensionToMime[extension]
	if !hasMime {
		mime = "application/x-octet-stream"
	}

	wrt.Header().Add("Content-Type", mime)
	wrt.Header().Add("Cache-Control", "max-age=240000")
	wrt.WriteHeader(200)
	wrt.Write(file)
}
