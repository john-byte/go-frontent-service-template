package types

type TExampleEntity struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}
