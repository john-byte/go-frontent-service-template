package linkedlist

type Node struct {
	Val  interface{}
	Next *Node
	Prev *Node
}

type List struct {
	head *Node
	tail *Node
}

func newImpl() List {
	return List{
		head: nil,
		tail: nil,
	}
}

func New() List {
	return newImpl()
}

func (self *List) AppendVal(val interface{}) {
	node := Node{
		Val:  val,
		Next: nil,
		Prev: nil,
	}

	if self.head == nil || self.tail == nil {
		self.head = &node
		self.tail = &node
	} else {
		node.Prev = self.tail
		self.tail.Next = &node
		self.tail = self.tail.Next
	}
}

func (self *List) RemoveNode(node *Node) bool {
	if self.head == nil || self.tail == nil {
		return false
	}

	if node == self.head {
		self.head = self.head.Next
		if self.head != nil {
			self.head.Prev = nil
		} else {
			self.tail = nil
		}
		return true
	}

	if node == self.tail {
		self.tail = self.tail.Prev
		self.tail.Next = nil
		return true
	}

	prev := node.Prev
	next := node.Next
	node.Prev.Next = next
	node.Next.Prev = prev
	return true
}

func (self *List) Head() *Node {
	return self.head
}

func (self *List) Tail() *Node {
	return self.tail
}
