package trie

type TrieNode struct {
	children    map[rune]*TrieNode
	isEndOfWord bool
}

type Trie struct {
	root *TrieNode
}

func New() Trie {
	root := TrieNode{
		children:    make(map[rune]*TrieNode, 0),
		isEndOfWord: false,
	}

	return Trie{
		root: &root,
	}
}

func (self *Trie) PutKey(key []rune) {
	node := self.root

	for _, ch := range key {
		if _, hasChild := node.children[ch]; !hasChild {
			node.children[ch] = &TrieNode{
				children:    make(map[rune]*TrieNode, 0),
				isEndOfWord: false,
			}
		}

		node = node.children[ch]
	}

	node.isEndOfWord = true
}

func (self *Trie) GetEndOfPrefix(stream []rune) int {
	node := self.root

	for i, ch := range stream {
		if _, hasChild := node.children[ch]; !hasChild {
			if !node.isEndOfWord {
				return -1
			}

			return i
		}

		node = node.children[ch]
	}

	if !node.isEndOfWord {
		return -1
	}

	return len(stream)
}
