package lrucache

import (
	orderedmap "go-frontend-service-template/utils/ordered_map"
)

type LruCache struct {
	entries  orderedmap.OrderedMap
	capacity uint64
	length   uint64
}

func New(capacity uint64) LruCache {
	return LruCache{
		entries:  orderedmap.New(),
		capacity: capacity,
		length:   0,
	}
}

func (self *LruCache) propagateEntry(key interface{}, val interface{}) {
	self.entries.Delete(key)
	self.entries.Put(key, val)
}

func (self *LruCache) Get(key interface{}) (interface{}, bool) {
	val, hasKey := self.entries.Get(key)
	if !hasKey {
		return val, hasKey
	}

	self.propagateEntry(key, val)
	return val, true
}

func (self *LruCache) Put(key interface{}, val interface{}) {
	_, hadKey := self.entries.Get(key)
	self.propagateEntry(key, val)
	if hadKey {
		return
	}

	self.length++
	if self.length > self.capacity {
		begin := self.entries.BeginKV()
		self.entries.Delete(begin.Key)
		self.length--
	}
}

func (self *LruCache) Delete(key interface{}) bool {
	hadKey := self.entries.Delete(key)
	if hadKey {
		self.length--
	}
	return hadKey
}

func (self *LruCache) GetIterator() orderedmap.OrderedMapIterator {
	return self.entries.GetIterator()
}
