package orderedmap

import linkedlist "go-frontend-service-template/utils/linked_list"

type OrderedMap struct {
	dict      map[interface{}]interface{}
	nodesDict map[interface{}]*linkedlist.Node
	list      linkedlist.List
}

type KVPair struct {
	Key interface{}
	Val interface{}
}

type OrderedMapIterator struct {
	currNode *linkedlist.Node
}

func New() OrderedMap {
	res := OrderedMap{
		dict:      make(map[interface{}]interface{}),
		nodesDict: make(map[interface{}]*linkedlist.Node),
		list:      linkedlist.New(),
	}

	return res
}

func FromMap(rawMap map[interface{}]interface{}) OrderedMap {
	dict := make(map[interface{}]interface{})
	nodesDict := make(map[interface{}]*linkedlist.Node)
	list := linkedlist.New()

	for k, v := range rawMap {
		dict[k] = v
		list.AppendVal(KVPair{
			Key: k,
			Val: v,
		})
		nodesDict[k] = list.Tail()
	}

	return OrderedMap{
		dict:      dict,
		nodesDict: nodesDict,
		list:      list,
	}
}

func (self *OrderedMap) Keys() []interface{} {
	res := make([]interface{}, 0)
	iter := self.getIteratorImpl()
	for iter.HasNext() {
		res = append(res, iter.Next().Key)
	}
	return res
}

func (self *OrderedMap) Values() []interface{} {
	res := make([]interface{}, 0)
	iter := self.getIteratorImpl()
	for iter.HasNext() {
		res = append(res, iter.Next().Val)
	}
	return res
}

func (self *OrderedMap) Get(key interface{}) (interface{}, bool) {
	val, hasKey := self.dict[key]
	return val, hasKey
}

func (self *OrderedMap) Put(key interface{}, val interface{}) {
	_, hadKey := self.dict[key]
	self.dict[key] = val

	if hadKey {
		self.nodesDict[key].Val = KVPair{
			Key: key,
			Val: val,
		}
		return
	}

	self.list.AppendVal(KVPair{
		Key: key,
		Val: val,
	})
	self.nodesDict[key] = self.list.Tail()
}

func (self *OrderedMap) Delete(key interface{}) bool {
	_, hasKey := self.dict[key]
	if !hasKey {
		return false
	}

	delete(self.dict, key)
	node := self.nodesDict[key]
	self.list.RemoveNode(node)
	delete(self.nodesDict, key)

	return true
}

func (self *OrderedMap) getIteratorImpl() OrderedMapIterator {
	return OrderedMapIterator{
		currNode: self.list.Head(),
	}
}

func (self *OrderedMap) GetIterator() OrderedMapIterator {
	return self.getIteratorImpl()
}

func (self *OrderedMap) BeginKV() KVPair {
	return self.list.Head().Val.(KVPair)
}

func (self *OrderedMap) EndKV() KVPair {
	return self.list.Tail().Val.(KVPair)
}

func (self *OrderedMapIterator) HasNext() bool {
	return self.currNode != nil
}

func (self *OrderedMapIterator) Next() KVPair {
	res := self.currNode.Val
	self.currNode = self.currNode.Next
	return res.(KVPair)
}
