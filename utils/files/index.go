package files

func GetFileExtension(path string) string {
	ptr := len(path) - 1
	for ptr >= 0 && path[ptr] != '.' {
		ptr--
	}

	if ptr >= 0 {
		return path[ptr+1:]
	}

	return ""
}
