package httpheadersutils

import trie "go-frontend-service-template/utils/trie"

type TAcceptLanguageParser struct {
	validLangs *trie.Trie
}

var acceptLanguageTable map[string]string = map[string]string{
	"en":  "en",
	"ru":  "ru",
	"nk":  "nk",
	"ky":  "kg",
	"tr":  "tr",
	"hun": "hg",
	"es":  "es",
}

func NewAcceptLangParser() *TAcceptLanguageParser {
	validLangs := trie.New()
	validLangs.PutKey([]rune("en"))
	validLangs.PutKey([]rune("ru"))
	validLangs.PutKey([]rune("nk"))
	validLangs.PutKey([]rune("ky"))
	validLangs.PutKey([]rune("tr"))
	validLangs.PutKey([]rune("tr"))

	return &TAcceptLanguageParser{
		validLangs: &validLangs,
	}
}

func (self *TAcceptLanguageParser) GetLanguageCode(acceptLang string) string {
	ptr := 0
	acceptLangIter := []rune(acceptLang)

	for ptr < len(acceptLangIter) {
		endIdx := self.validLangs.GetEndOfPrefix(acceptLangIter[ptr:])
		if endIdx >= 0 {
			srcCode := acceptLangIter[ptr : ptr+endIdx]
			dstCode, hasDstCode := acceptLanguageTable[string(srcCode)]
			if !hasDstCode {
				return "ru"
			}
			return dstCode
		}
		ptr++
	}

	return "ru"
}
