package errorsutils

import (
	"fmt"
	"net/http"
	"time"
)

func LogError(
	moduleName string,
	methodName string,
	err error,
) error {
	nowTimestamp := time.Now().Format("2005-01-01 00:00:00")

	return fmt.Errorf(
		"[%v] [%v / %v] => %v",
		nowTimestamp,
		moduleName,
		methodName,
		err.Error(),
	)
}

func WriteErrToResponse(
	wrt http.ResponseWriter,
	err error,
	moduleName string,
	methodName string,
) {
	strErr := LogError(
		moduleName,
		methodName,
		fmt.Errorf(
			"\n + %v",
			err.Error(),
		),
	)

	wrt.WriteHeader(500)
	wrt.Write([]byte(strErr.Error()))
	fmt.Println(strErr)
}
