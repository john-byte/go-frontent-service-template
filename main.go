package main

import (
	"encoding/json"
	initjobs "go-frontend-service-template/init-jobs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"time"

	mux "github.com/gorilla/mux"
)

func main() {
	// Files storage
	jsonFile, err := os.ReadFile("creds.json")
	if err != nil {
		panic(err.Error())
	}

	var creds struct {
		FrontendBuildPath string `json:"frontendBuildPath"`
		IpcSecretKey      string `json:"ipcSecretKey"`
		MainBackendUrl    string `json:"mainBackendUrl"`
	}
	err = json.Unmarshal(jsonFile, &creds)
	if err != nil {
		panic(err.Error())
	}

	services, err := initjobs.InitServices(
		creds.FrontendBuildPath,
		creds.MainBackendUrl,
	)
	if err != nil {
		panic(err.Error())
	}

	rootRouter := mux.NewRouter()
	initjobs.InitInternalRoutes(
		rootRouter,
		services.PagesCache,
		creds.IpcSecretKey,
	)

	htmlPath := path.Join(
		creds.FrontendBuildPath,
		"index.html",
	)
	rawHtml, err := ioutil.ReadFile(htmlPath)
	if err != nil {
		panic(err.Error())
	}
	htmlBase := string(rawHtml)
	initjobs.InitSsrRoutes(
		rootRouter,
		creds.FrontendBuildPath,
		services.PagesCache,
		services.Interpreter,
		htmlBase,
		services.RpcService,
	)

	server := &http.Server{
		Handler:      rootRouter,
		Addr:         "localhost:8003",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	log.Println("Server is listening!")
	log.Fatal(server.ListenAndServe())
}
