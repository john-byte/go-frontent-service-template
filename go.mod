module go-frontend-service-template

go 1.18

require (
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/dop251/goja v0.0.0-20221118162653-d4bf6fde1b86 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)
