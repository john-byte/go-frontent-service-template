package pagescache

import (
	lrucache "go-frontend-service-template/utils/lru_cache"
	"sync"
	"time"
)

type TPagesCacheEntry struct {
	Content   string
	ExpiredAt time.Time
}

type TPagesCache struct {
	pages     *lrucache.LruCache
	pagesLock *sync.Mutex
	timer     *time.Ticker
}

func New() *TPagesCache {
	cache := lrucache.New(10)
	return &TPagesCache{
		pages:     &cache,
		pagesLock: &sync.Mutex{},
		timer:     nil,
	}
}

func (self *TPagesCache) GetPage(pathWQuery string) (string, bool) {
	self.pagesLock.Lock()
	defer self.pagesLock.Unlock()

	page, hasPage := self.pages.Get(pathWQuery)
	if !hasPage {
		return "", false
	}

	pageEntry := page.(TPagesCacheEntry)
	return pageEntry.Content, true
}

func (self *TPagesCache) PutPage(
	pathWQuery string,
	pageContent string,
	pageDuration time.Duration,
) {
	self.pagesLock.Lock()
	defer self.pagesLock.Unlock()

	self.pages.Put(
		pathWQuery,
		TPagesCacheEntry{
			Content:   pageContent,
			ExpiredAt: time.Now().Add(pageDuration),
		},
	)
}

func (self *TPagesCache) InvalidatePage(pathWQuery string) {
	self.pagesLock.Lock()
	self.pages.Delete(pathWQuery)
	self.pagesLock.Unlock()
}

func (self *TPagesCache) InvalidatePages(pathsWQuery []string) {
	self.pagesLock.Lock()
	defer self.pagesLock.Unlock()

	for _, page := range pathsWQuery {
		self.pages.Delete(page)
	}
}

func (self *TPagesCache) RunTrashClock() {
	self.timer = time.NewTicker(time.Minute * 2)

	go func() {
		for {
			<-self.timer.C

			now := time.Now()

			self.pagesLock.Lock()
			pagesIter := self.pages.GetIterator()
			deletedPages := make([]string, 0)
			for pagesIter.HasNext() {
				kv := pagesIter.Next()
				pageEntry := kv.Val.(TPagesCacheEntry)
				if now.After(pageEntry.ExpiredAt) {
					deletedPages = append(deletedPages, kv.Key.(string))
				}
			}
			for _, dPage := range deletedPages {
				self.pages.Delete(dPage)
			}
			self.pagesLock.Unlock()
		}
	}()
}
