package jsinterpreter

import (
	"errors"
	"fmt"
	errorsutils "go-frontend-service-template/utils/errors"
	"io/ioutil"
	"path/filepath"

	jsvm "github.com/dop251/goja"
	jsparser "github.com/dop251/goja/parser"
)

type TJsFunction func(args ...interface{}) interface{}

type TJsInterpreter struct {
	frontendBuildPath string
	interp            *jsvm.Runtime
}

func New(frontendBuildPath string) *TJsInterpreter {
	interp := jsvm.New()
	interp.SetParserOptions(
		jsparser.WithDisableSourceMaps,
	)

	return &TJsInterpreter{
		frontendBuildPath: frontendBuildPath,
		interp:            interp,
	}
}

func (self *TJsInterpreter) Init() error {
	err := self.interp.Set("window", jsvm.Undefined())
	if err != nil {
		return errorsutils.LogError(
			"JsInterpreter",
			"Init",
			err,
		)
	}

	err = self.interp.Set("ssrExports", map[string]TJsFunction{})
	if err != nil {
		return errorsutils.LogError(
			"JsInterpreter",
			"Init",
			err,
		)
	}

	ssrImports := map[string]interface{}{
		"log": func(args ...interface{}) interface{} {
			if len(args) < 1 {
				return nil
			}

			fmt.Printf("[LOG-FROM-JS-MODULE] => %v\n", args[0])
			return nil
		},
	}
	err = self.interp.Set("ssrImports", ssrImports)
	if err != nil {
		return errorsutils.LogError(
			"JsInterpreter",
			"Init",
			err,
		)
	}

	jsFilePath := filepath.Join(
		self.frontendBuildPath,
		"server",
		"js",
		"build.js",
	)
	jsFile, err := ioutil.ReadFile(jsFilePath)
	if err != nil {
		return errorsutils.LogError(
			"JsInterpreter",
			"Init",
			err,
		)
	}
	jsFileStr := string(jsFile)

	_, err = self.interp.RunString(jsFileStr)
	if err != nil {
		return errorsutils.LogError(
			"JsInterpreter",
			"Init",
			err,
		)
	}

	return nil
}

func (self *TJsInterpreter) ExecExtFunction(rawFnName string, fnArgs ...interface{}) (interface{}, error) {
	selfRawObjSym := self.interp.Get("ssrExports")
	selfRawObj := selfRawObjSym.Export()
	selfObj, isFunctionTable := selfRawObj.(map[string]TJsFunction)
	if !isFunctionTable {
		return nil, errorsutils.LogError(
			"JsInterpreter",
			"ExecExtFunction",
			errors.New("self is not function table"),
		)
	}

	fn := selfObj[rawFnName]
	val := fn(fnArgs...)
	return val, nil
}

func (self *TJsInterpreter) assignGlobalImport(globalName string, value interface{}) error {
	ssrImportsSym := self.interp.Get("ssrImports")
	rawSsrImports := ssrImportsSym.Export()
	ssrImports, isTable := rawSsrImports.(map[string]interface{})
	if !isTable {
		return errorsutils.LogError(
			"JsInterpreter",
			"assignGlobalImport",
			errors.New("ssrImports is not a table"),
		)
	}
	ssrImports[globalName] = value
	err := self.interp.Set("ssrImports", ssrImports)
	if err != nil {
		return errorsutils.LogError(
			"JsInterpreter",
			"assignGlobalImport",
			err,
		)
	}

	return nil
}

func (self *TJsInterpreter) SetGlobal(globalName string, value interface{}) (func() error, error) {
	err := self.assignGlobalImport(globalName, value)
	if err != nil {
		return nil, errorsutils.LogError(
			"JsInterpreter",
			"SetGlobal",
			err,
		)
	}

	return func() error {
		err := self.assignGlobalImport(globalName, nil)
		if err != nil {
			return errorsutils.LogError(
				"JsInterpreter",
				"SetGlobal",
				err,
			)
		}
		return nil
	}, nil
}
