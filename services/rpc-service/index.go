package rpcservice

import (
	"encoding/json"
	"errors"
	"fmt"
	types "go-frontend-service-template/types"
	errorsutils "go-frontend-service-template/utils/errors"
	"io/ioutil"
	"net/http"
)

type TRpcService struct {
	mainBaseUrl   string
	offersBaseUrl string
}

func New(mainBaseUrl string) *TRpcService {
	return &TRpcService{
		mainBaseUrl: mainBaseUrl,
	}
}

func (self *TRpcService) GetExampleResource() ([]types.TExampleEntity, error) {
	reqUrl := fmt.Sprintf(
		"%v/fetch-entities",
		self.offersBaseUrl,
	)

	response, err := http.Get(reqUrl)
	if err != nil {
		return nil, errorsutils.LogError(
			"RpcService",
			"GetExampleResource",
			err,
		)
	}

	rawBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, errorsutils.LogError(
			"RpcService",
			"GetExampleResource",
			err,
		)
	}
	if response.StatusCode >= 400 {
		return nil, errorsutils.LogError(
			"RpcService",
			"GetExampleResource",
			errors.New(string(rawBody)),
		)
	}

	var entities []types.TExampleEntity
	err = json.Unmarshal(rawBody, &entities)
	if err != nil {
		return nil, errorsutils.LogError(
			"RpcService",
			"GetExampleResource",
			err,
		)
	}

	return entities, nil
}
