package initjobs

import (
	// Services
	jsinterpreter "go-frontend-service-template/services/jsinterpreter"
	pagescache "go-frontend-service-template/services/pages-cache"
	rpcservice "go-frontend-service-template/services/rpc-service"

	// Controllers
	cacheinvalidatorcontroller "go-frontend-service-template/controllers/cache-invalidator-controller"
	filecontroller "go-frontend-service-template/controllers/file-controller"
	pagecontroller "go-frontend-service-template/controllers/page-render-controller"

	// Modules
	"net/http"

	mux "github.com/gorilla/mux"
	fileutils "go-frontend-service-template/utils/files"
)

func InitInternalRoutes(
	rootRouter *mux.Router,
	pagesCache *pagescache.TPagesCache,
	ipcSecretKey string,
) {
	cacheInvalidateController := cacheinvalidatorcontroller.New(
		pagesCache,
		ipcSecretKey,
	)

	internalRouter := rootRouter.PathPrefix("/internal").Subrouter()
	internalRouter.
		Methods("POST").
		Headers("x-ipc-secret").
		Path("/invalidate-example").
		HandlerFunc(
			func(rw http.ResponseWriter, r *http.Request) {
				cacheInvalidateController.InvalidateExamplePage(rw, r)
			},
		)
}

func InitSsrRoutes(
	rootRouter *mux.Router,
	frontendBuildPath string,
	pagesCache *pagescache.TPagesCache,
	jsInterpreter *jsinterpreter.TJsInterpreter,
	htmlBase string,
	rpcService *rpcservice.TRpcService,
) {
	fileController := filecontroller.New(frontendBuildPath)
	pageController := pagecontroller.New(
		pagesCache,
		jsInterpreter,
		htmlBase,
		rpcService,
	)

	frontendRouter := rootRouter.
		PathPrefix("/").
		Subrouter()
	frontendRouter.
		Methods("GET").
		Path("/pageA").
		HandlerFunc(
			func(rw http.ResponseWriter, r *http.Request) {
				pageController.RenderExamplePageA(rw, r)
			},
		)
	frontendRouter.
		Methods("GET").
		PathPrefix("/").
		HandlerFunc(
			func(rw http.ResponseWriter, r *http.Request) {
				// If index page is same as pageA
				if r.URL.Path == "/" {
					pageController.RenderExamplePageA(rw, r)
				} else {
					fileExtension := fileutils.GetFileExtension(r.URL.Path)
					if len(fileExtension) > 0 {
						fileController.FetchFile(rw, r)
					} else {
						pageController.RenderDefaultPage(rw, r)
					}
				}
			},
		)
}
