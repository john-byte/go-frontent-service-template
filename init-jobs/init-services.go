package initjobs

import (
	jsinterpreter "go-frontend-service-template/services/jsinterpreter"
	pagescache "go-frontend-service-template/services/pages-cache"
	rpcservice "go-frontend-service-template/services/rpc-service"
	errorsutils "go-frontend-service-template/utils/errors"
)

type TServicesResources struct {
	Interpreter *jsinterpreter.TJsInterpreter
	PagesCache  *pagescache.TPagesCache
	RpcService  *rpcservice.TRpcService
}

func InitServices(
	frontendBuildPath string,
	mainBaseUrl string,
) (*TServicesResources, error) {
	interpreter := jsinterpreter.New(frontendBuildPath)
	err := interpreter.Init()
	if err != nil {
		return nil, errorsutils.LogError(
			"InitJobs",
			"InitServices",
			err,
		)
	}

	pagesCache := pagescache.New()
	pagesCache.RunTrashClock()

	rpcService := rpcservice.New(mainBaseUrl)

	return &TServicesResources{
		Interpreter: interpreter,
		PagesCache:  pagesCache,
		RpcService:  rpcService,
	}, nil
}
